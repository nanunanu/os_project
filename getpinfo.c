#include "types.h"
#include "stat.h"
#include "user.h"
#include "pstat.h"

int
main(int argc, char *argv[])
{
   struct pstat st;
   getpinfo(&st);
   
   int i;
   for(i = 0; i < NPROC; i++)
  {   
     {
      printf(1, "pid: %d | tickets: %d | ticks: %d | inuse: %d \n", st.pid[i], st.tickets[i], st.ticks[i], st.inuse[i]);
   }
  }
   exit();
}


